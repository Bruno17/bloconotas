package com.example.bloconotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    Button buttonSalvar;
    private TextView textResultado;
    private TextInputEditText editText;
    private static final String PREFERENCIAS_FILE = "Notas";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonSalvar = findViewById(R.id.buttonSalvar);
        editText = findViewById(R.id.editText);
        textResultado = findViewById(R.id.textResultado);

        preferences = getSharedPreferences(PREFERENCIAS_FILE, MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("teste", "BabyShark thuturu babyShark");
        editor.commit();
        Toast.makeText(this, preferences.getString("teste", "Erro ao recuperar a String"), Toast.LENGTH_LONG).show();


    }
}